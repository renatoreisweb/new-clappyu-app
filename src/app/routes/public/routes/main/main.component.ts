import { Component, OnInit, ViewChild } from '@angular/core';
import { CarouselDirective } from '../../../../shared/directives/carousel.directive';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {


  public items = [
    {
      color: '#377ef1',
      image: 'assets/images/main_bg_slide_1.png'
    },
    {
      color: '#d472ec',
      image: 'assets/images/main_bg_slide_1.png'
    },
    {
      color: '#e95656',
      image: 'assets/images/main_bg_slide_1.png'
    }
  ]

  @ViewChild('carousel', {read: CarouselDirective}) public carousel: CarouselDirective;




  constructor() {
   }

  ngOnInit() {
  }

  select(i){
    this.carousel.select(i);
  }

  get selectedIndex(){
    return this.carousel.selectedIndex;
  }

}
