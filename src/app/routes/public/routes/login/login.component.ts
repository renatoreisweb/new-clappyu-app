import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../../services/api.service';
import { Router } from '@angular/router';
import { UserService } from '../../../../services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public disabled = false;
  public valid = false;
  constructor(private api: ApiService, private router: Router, private userService: UserService) { }

  ngOnInit() {
  }

  send(data){
    this.disabled = true;

    this.userService.login(data).then(()=>{
      this.router.navigate(['/private/home']);
    }).catch(()=>{

    });
  }
}
