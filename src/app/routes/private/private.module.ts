import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrivateComponent } from './private.component';
import { Route, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { TalksModule } from './routes/talks/talks.module';
import { DiscoverModule } from './routes/discover/discover.module';
import { EventsModule } from './routes/events/events.module';
import { HomeModule } from './routes/home/home.module';
import { ProfileModule } from './routes/profile/profile.module';
import { SelfEventsModule } from './routes/self-events/self-events.module';
import { ChatModule } from './routes/chat/chat.module';
import { SettingsModule } from './routes/settings/settings.module';

@NgModule({
  declarations: [PrivateComponent],
  imports: [
    CommonModule,
    HomeModule,
    RouterModule,
    SharedModule,
    TalksModule,
    DiscoverModule,
    EventsModule,
    ProfileModule,
    SelfEventsModule,
    ChatModule,
    SettingsModule
  ]
})
export class PrivateModule { 

  public static route: Route = {
    path: 'private',
    component: PrivateComponent,
    resolve: [],
    children: [
      ProfileModule.route,
      HomeModule.route,
      TalksModule.route,
      DiscoverModule.route,
      EventsModule.route,
      SelfEventsModule.route,
      ChatModule.route,
      SettingsModule.route,
      { path: '**', redirectTo: '/private/home', pathMatch: 'full' },
      { path: '', redirectTo: '/private/home', pathMatch: 'full' },
    ]
  };
}
