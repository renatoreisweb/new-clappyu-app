import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../../services/user.service';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss']
})
export class EventsComponent implements OnInit {

  public markers = [
    {
      position:{
        latitude: -23.539419,
        longitude: -46.831120
      },
      icon: 'assets/images/point_event.png',
      title: 'Event 1',
    },
    {
      position:{
        latitude: -23.538438,
        longitude: -46.829862
      },
      icon: 'assets/images/point_event.png',
      title: 'Event 2',
    },
    {
      position:{
        latitude: -23.536854,
        longitude: -46.833950
      },
      icon: 'assets/images/point_event.png',
      title: 'Event 3',
    },
    {
      position:{
        latitude: -23.533431,
        longitude: -46.827859
      },
      icon: 'assets/images/point_event.png',
      title: 'Event 4',
    },
    {
      position:{
        latitude: -23.531925,
        longitude: -46.827822
      },
      icon: 'assets/images/point_event.png',
      title: 'Event 5',
    }
  ];

  constructor(private userService: UserService) { }

  ngOnInit() {



    (async ()=>{
      var response = await this.userService.request('/events/opportunities','get');
      console.log(response);
    })().catch(()=>{

    })

  }

}
