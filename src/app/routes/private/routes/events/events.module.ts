import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventsComponent } from './events.component';
import { Route } from '@angular/router';
import { SharedModule } from '../../../../shared/shared.module';

@NgModule({
  declarations: [EventsComponent],
  imports: [
    CommonModule,
    SharedModule
  ]
})
export class EventsModule { 
  public static route: Route = {
    path: 'events',
    component: EventsComponent,
    resolve: [],
    children: [
 
    ]
  };
}
