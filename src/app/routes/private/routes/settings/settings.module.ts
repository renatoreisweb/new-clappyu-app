import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingsComponent } from './settings.component';
import { Route } from '@angular/router';
import { SharedModule } from '../../../../shared/shared.module';

@NgModule({
  declarations: [SettingsComponent],
  imports: [
    CommonModule,
    SharedModule
  ]
})
export class SettingsModule {
  public static route: Route = {
    path: 'settings',
    component: SettingsComponent,
    resolve: [],
    children: [
 
    ]
  };
}
