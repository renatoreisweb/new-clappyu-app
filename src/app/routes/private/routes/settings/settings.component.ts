import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../../services/api.service';
import { LanguageService } from '../../../../services/language.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
  public disabled = true;
  public valid = false;
  public languages = [];
  constructor(private api: ApiService, private languageService: LanguageService, private apiService: ApiService) {
    this.apiService.request('/languages',{method: 'get'}).then((response)=>{
      response.data.data.forEach(element => {
          this.languages.push({
            text: element.nome,
            value: element.locale
          });
      });
      this.disabled = false;
     });
  }

  ngOnInit() {

  }

  get language(){
    return this.languageService.language;
  }

  send(data){
    this.disabled = true;
    this.languageService.set(data.lang).then(()=>{
      history.back();
    });
  }
}
