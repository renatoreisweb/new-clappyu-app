import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../../services/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public events = [''];
  public users = [];
  public loaderUsers = true;
  public loaderEvents = true;


  constructor(private userService: UserService) { }

  ngOnInit() {
    this.getUsers();
    this.getEvents();
  }


  getUsers() {
    this.loaderUsers = true;
    this.userService.request('/search', 'post',{id_tipo: 1, per_page: 5}).then((response) => {
      this.users = response.data.data;
      setTimeout(()=>{
        this.loaderUsers = false;
      },200);
    });
  }


  getEvents() {
    this.loaderEvents = true;
    setTimeout(()=>{
      this.loaderEvents = false;
    },200);
  }
}
