import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { Route, RouterModule } from '@angular/router';
import { SharedModule } from '../../../../shared/shared.module';
import { CardUserComponent } from './components/card-user/card-user.component';

@NgModule({
  declarations: [HomeComponent, CardUserComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule
  ]
})
export class HomeModule {
  public static route: Route = {
    path: 'home',
    component: HomeComponent,
    data: {
      cache: true
    },
    resolve: [],
    children: [
 
    ]
  };
}
