import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelfEventsComponent } from './self-events.component';

describe('SelfEventsComponent', () => {
  let component: SelfEventsComponent;
  let fixture: ComponentFixture<SelfEventsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelfEventsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelfEventsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
