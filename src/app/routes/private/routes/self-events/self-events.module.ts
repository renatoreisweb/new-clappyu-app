import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelfEventsComponent } from './self-events.component';
import { Route } from '@angular/router';
import { SharedModule } from '../../../../shared/shared.module';

@NgModule({
  declarations: [SelfEventsComponent],
  imports: [
    CommonModule,
    SharedModule
  ]
})
export class SelfEventsModule {
  public static route: Route = {
    path: 'self-events',
    component: SelfEventsComponent,
    resolve: [],
    children: []
  };
}
