import { Component, OnInit, Input } from '@angular/core';
import { Chat } from '../../../../../../classes/chat.class';
import { UserService } from '../../../../../../services/user.service';

@Component({
  selector: 'app-talk-card',
  templateUrl: './talk-card.component.html',
  styleUrls: ['./talk-card.component.scss']
})
export class TalkCardComponent implements OnInit {


  @Input() public chat: Chat;


  constructor(private userService: UserService) { 
   
  }

  ngOnInit() {
  }


  get names(){
    if(this.chat.event){

    } else{
      const names = [];
      this.chat.participants.forEach((participant)=>{
          if(participant.username)names.push(participant.username);
      });
      return names.join(', ');
    }
  }

  get avatares(){
    if(this.chat.event){

    } else{
      const avatares = [];
      this.chat.participants.forEach((participant, i)=>{
        if(i < 4){
          avatares.push(participant.avatar);
        }
      });
      return avatares;
    }
  }

}
