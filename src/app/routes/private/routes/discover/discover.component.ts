import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../../services/user.service';
import { FiltersService } from '../../../../services/filters.service';

@Component({
  selector: 'app-discover',
  templateUrl: './discover.component.html',
  styleUrls: ['./discover.component.scss']
})
export class DiscoverComponent implements OnInit {

  public items = [];
  public loader = true;
  public filters = this.filtersService.get('discover');

  constructor(private userService: UserService, private filtersService: FiltersService) { }

  ngOnInit() {

    this.find();

    this.filtersService.change.subscribe(()=>{
        this.filters = this.filtersService.get('discover');
        this.find();
    });

  }

  find() {
    this.items = [];
    this.loader = true;

    const body = {per_page: 100};

    if(this.filters){
      for(let key in this.filters){
        const val = this.filters[key];
        if(val != undefined && val != null) body[key] = val;
      }
    }

    console.log(body);
    this.userService.request('/search', 'post',body).then((response) => {

      this.items = response.data.data;
      console.log(this.items);

      this.loader = false;
    });
  }

}
