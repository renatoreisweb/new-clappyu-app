import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DiscoverComponent } from './discover.component';
import { Route, RouterModule } from '@angular/router';
import { SharedModule } from '../../../../shared/shared.module';
import { CardPersonComponent } from './components/card-person/card-person.component';

@NgModule({
  declarations: [DiscoverComponent, CardPersonComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule
  ]
})
export class DiscoverModule {
  public static route: Route = {
    path: 'discover',
    component: DiscoverComponent,
    data: {
      cache: true
    },
    resolve: [],
    children: []
  };
 }
