import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-card-person',
  templateUrl: './card-person.component.html',
  styleUrls: ['./card-person.component.scss']
})
export class CardPersonComponent implements OnInit {

  @Input() public compact = false;
  @Input() public name = null;
  @Input() public avatar = null;
  @Input() public cover = null;
  @Input() public type;
  @Input() public rating;
  @Input() public views;
  @Input() public likes;



  constructor() { }

  ngOnInit() {
  }

}
