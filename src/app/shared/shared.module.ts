import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MoneyPipe } from './pipes/money.pipe';
import { CarouselDirective } from './directives/carousel.directive';
import { CarouselItemDirective } from './directives/carousel-item.directive';
import { FormsModule } from '@angular/forms';
import { FormComponent } from './components/form/form.component';
import { ButtonComponent } from './components/button/button.component';
import { HeaderComponent } from './components/header/header.component';
import { ContentComponent } from './components/content/content.component';
import { ViewComponent } from './components/view/view.component';
import { ContentHeaderComponent } from './components/content-header/content-header.component';
import { TabsComponent } from './components/tabs/tabs.component';
import { RadioComponent } from './components/radio/radio.component';
import { InputComponent } from './components/input/input.component';
import { FieldsetComponent } from './components/fieldset/fieldset.component';
import { TabComponent } from './components/tab/tab.component';
import { MapComponent } from './components/map/map.component';
import { CardEventComponent } from './components/card-event/card-event.component';
import { UserInfoComponent } from './components/user-info/user-info.component';
import { ImageChangeComponent } from './components/image-change/image-change.component';
import { UserImageComponent } from './components/user-image/user-image.component';
import { ImgComponent } from './components/img/img.component';


@NgModule({
  declarations: [
    MoneyPipe,
    CarouselDirective,
    CarouselItemDirective,
    FormComponent,
    ButtonComponent,
    HeaderComponent,
    ContentComponent,
    ViewComponent,
    ContentHeaderComponent,
    TabsComponent,
    RadioComponent,
    InputComponent,
    FieldsetComponent,
    TabComponent,
    MapComponent,
    CardEventComponent,
    UserInfoComponent,
    ImageChangeComponent,
    UserImageComponent,
    ImgComponent
  ],
  exports: [
    MoneyPipe,
    CarouselDirective,
    CarouselItemDirective,
    FormComponent,
    ButtonComponent,
    HeaderComponent,
    ContentComponent,
    ViewComponent,
    ContentHeaderComponent,
    TabsComponent,
    RadioComponent,
    InputComponent,
    FieldsetComponent,
    TabComponent,
    MapComponent,
    CardEventComponent,
    UserInfoComponent,
    ImageChangeComponent,
    UserImageComponent,
    ImgComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule
  ]
})
export class SharedModule { }


