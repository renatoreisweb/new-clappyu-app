import { Directive, OnInit, OnDestroy, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import * as Flickity from 'flickity';
@Directive({
  selector: '[carousel]'
})
export class CarouselDirective implements OnInit, OnDestroy {
  public slider: any;
  public items = [];
  private timer;
  public options = {};
  private updateVisual;

  @Output('carousel.scroll') eventScroll = new EventEmitter();
  @Output('carousel.init') eventInit = new EventEmitter();


  @Input('carousel') private set bvpCarousel(options) {
    this.options = options;
    this.init();
  };


  constructor(public element: ElementRef) {
    this.slider = new Flickity(this.element.nativeElement, this.options);
  }

  setEvents() {
    // this.slider.on('scroll', ( progress) => {
    //   progress = Math.max(0, Math.min(1, progress));
    //   this.eventScroll.emit(progress);
    // });
  }

  init() {
    setTimeout(() => {
      if (this.slider) {
        this.slider.destroy();
      }
      this.slider = new Flickity(this.element.nativeElement, this.options);
      this.setEvents();
      this.updateVisual();
      setTimeout(() => {
        this.eventInit.emit();
      });

    });
  }

  get selectedIndex() {
    return this.slider.selectedIndex;
  }

  public append(item) {

    const index = this.items.indexOf(item);
    if (index < 0) {
      this.slider.append(item);
      this.updateVisual();
    }
  }

  remove(item) {
    const index = this.items.indexOf(item);
    if (index > -1) {
      this.items.splice(index, 1);
    }
  }

  select(index, isWrapped?, isInstant?){
    this.slider.select( index, isWrapped, isInstant );
  }


  ngOnInit() {
    this.updateVisual = ()=>{
      if(this.timer){
        clearTimeout(this.timer);
      }
      this.timer = setTimeout(()=>{
        this.slider.resize();
      }, 50);
    }
    window.addEventListener('resize', this.updateVisual);
  }
  ngOnDestroy() {
    if(this.timer){
      clearTimeout(this.timer);
    }
    window.removeEventListener('resize', this.updateVisual);
  }

}
