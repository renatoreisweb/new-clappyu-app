import { Component, OnInit, ElementRef, OnDestroy, Input } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit, OnDestroy {

  @Input() type = 'button';
  @Input() animation = false;

  public pressed = false;
  public touchend;
  public style: any = {};

  constructor(private element: ElementRef) {

    this.touchend = () => {

    }

    element.nativeElement.addEventListener('touchstart', (e) => {
      var rect = element.nativeElement.getBoundingClientRect();
      const finger = e.touches[0];
      this.style.left = (finger.clientX - rect.left) + 'px';
      this.style.top = (finger.clientY - rect.top) + 'px';
      this.pressed = false;
      setTimeout(() => {
        this.pressed = true;
      });
    });

    window.addEventListener('touchend', this.touchend);
  }

  ngOnInit() {

  }

  ngOnDestroy() {
    window.removeEventListener('touchend', this.touchend);
  }

}
