import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss']
})
export class UserInfoComponent implements OnInit {

  @Input() public compact = false;
  @Input() public avatar;
  @Input() public name;
  @Input() public type;
  @Input() public rating;
  @Input() public views;
  @Input() public likes;



  

  constructor() { }

  ngOnInit() {
  }

  ratingEdit(str){
    return Number(str).toFixed(1);
  }

}
