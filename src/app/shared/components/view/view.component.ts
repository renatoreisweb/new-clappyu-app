import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {

  @Input() public title: string;
  @Input() public header = 'basic';
  @Input() public content = true;
  @Input() public users: {avatares: string; names: string; msg: string};


  constructor() { }

  ngOnInit() {
  }

}
