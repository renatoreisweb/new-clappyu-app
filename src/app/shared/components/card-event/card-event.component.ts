import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-card-event',
  templateUrl: './card-event.component.html',
  styleUrls: ['./card-event.component.scss']
})
export class CardEventComponent implements OnInit {

  @Input() compact = false;

  constructor() { }

  ngOnInit() {
  }

}
