import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { MenuService } from '../../../services/menu.service';
import { ModalService } from '../../../services/modal.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input() public title: string;
  @Input() public type = 'basic';
  @Input() public users: { avatares: string; names: string; msg: string };

  constructor(private menuService: MenuService, private modalService: ModalService) { }

  ngOnInit() {
  }

  back() {
    history.back();
  }

  open() {
    this.menuService.open();
  }

  filtersDiscover(){
    this.modalService.open('filter-discover', {});
  }

}
