import { Component, EventEmitter, Output, Input } from '@angular/core';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent {
  public fields = [];
  @Output() send = new EventEmitter();
  @Output() valid = new EventEmitter();
  @Output() invalid = new EventEmitter();
  @Input() disabled: boolean;
  constructor() { }

  public add(input) {
    this.fields.push(input);
    this.change();
  }

  remove(input) {
    const index = this.fields.indexOf(input);
    if (index > -1) {
      this.fields.splice(index, 1);
    }
    this.change();
  }

  onSubmit($event) {
    $event.preventDefault();
    $event.stopPropagation();
    if (this.disabled || !this.valid) {
      return false;
    }
    const data = {};
    this.fields.forEach((component) => {
      if (!component.disabled) {
        data[component.name] = component._value === undefined ? null : component._value;
      }
    });
    this.send.emit(data);
    return false;
  }

  change() {
    setTimeout(() => {
      for (const component of this.fields) {
        if (!component.disabled && (!component.name || !component.valid)) {
          this.invalid.emit();
          return;
        }
      }
      this.valid.emit();
    });
  }
}
