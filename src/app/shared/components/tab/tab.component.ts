import { Component, OnInit, OnDestroy, Optional, ElementRef, Host, Input } from '@angular/core';
import { TabsComponent } from '../tabs/tabs.component';

@Component({
  selector: 'app-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.scss']
})
export class TabComponent implements OnInit, OnDestroy {

  @Input() title: string;
  @Input() icon: string;

  constructor(public element: ElementRef, @Optional() @Host() private parent?: TabsComponent) {

  }

  ngOnInit(): void {
    if (!this.parent) {
      return;
    }

    this.parent.append(this);
  }
  ngOnDestroy() {
    if (this.parent) {
      this.parent.remove(this);
    }
  }
}