import { Component, Input, ViewChild } from '@angular/core';
import { Formable } from '../../../classes/formable.class';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})
export class InputComponent extends Formable {

  public focus = false;
  public _options = [];



  @Input() public placeholder: string;
  @Input() public type: string;
  @Input() set options(val){
    this._options = val;
  };
}
