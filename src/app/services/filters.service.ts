import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FiltersService {

  private filters = {

  }

  public change = new EventEmitter();

  constructor() { }

  set(name, filters){
    this.filters[name] = filters;
    this.change.emit(name);
  }

  get(name){
    return this.filters[name] || null;
  }
  
}
