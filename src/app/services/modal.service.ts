import { Injectable, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ModalService {
  public scope;
  public change = new EventEmitter();
  constructor(private router: Router) { }

  open(modal, scope?){
    this.scope = scope;
    this.change.emit();
    setTimeout(() => {
      this.router.navigate( [this.router.url], {fragment: modal});
    });
  }
}
