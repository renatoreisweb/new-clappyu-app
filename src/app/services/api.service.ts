import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { LanguageService } from './language.service';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private languageService: LanguageService) { }


  async request(url, conf?){

    const config: any = {
      method: conf.method,
      headers: {
        'Accept-Language': this.languageService.language,
       'Accept': 'q=0.8;application/json;q=0.9',
        'Content-Type': 'application/json',
      }
    };

    if(conf.Authorization){
      config.headers.Authorization = conf.Authorization;
    }
    
    if(conf.body){
      if(conf.body instanceof FormData){
        config.body = conf.body;
        delete config.headers['Accept'];
        delete config.headers['Content-Type'];

      }else{
        config.body = JSON.stringify(conf.body);

      }
    }
    
    const response = await fetch(`${environment.apiURL}${url}`, config);
    const json = await response.json();

    return {
      statusCode: response.status,
      data: json
    }
  }
  
}
