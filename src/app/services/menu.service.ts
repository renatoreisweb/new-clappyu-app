import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MenuService {
  private _isOpen = false;

  public change = new EventEmitter();

  constructor() { }

  close() {
    setTimeout(() => {
      this._isOpen = false;
      this.change.emit();
    },50);
  }

  toggle() {
    setTimeout(() => {
      this._isOpen = !this._isOpen;
      this.change.emit();
    },50);
  }

  open() {
    setTimeout(() => {
      this._isOpen = true;
      this.change.emit();
    },50);

  }

  get isOpen() {
    return this._isOpen;
  }
}
