import { Input, Output, EventEmitter, OnInit, OnDestroy, Host, Optional, ElementRef, ViewChild } from '@angular/core';
import { Validator } from 'jsonschema';
import { FormComponent } from '../shared/components/form/form.component';

export class Formable implements OnInit, OnDestroy {

  static validator = new Validator();
  public _value;
  public valid = true;
  public pristine = true;
  public error: string;
  public errorArgument: any;

  @Input() public name: string;
  @Input() public disabled = false;
  @Input() public schema: any;
  @Output() valueChange = new EventEmitter();

  @Input() set value(val) {
    this._value = val;

    this.valid = true;

    if (!this.disabled) {
      if (this.schema) {
        const status = Formable.validator.validate(val, this.schema);
        if (!status.valid) {
          this.error = status.errors[0].name;
          this.errorArgument = status.errors[0].argument;
          this.valid = false;
        } else {
          this.valid = true;
        }
      }
    }

    if (this.parent) this.parent.change();
    this.valueChange.emit(this._value);
  }

  get value() {
    return this._value;
  }

  constructor(public element: ElementRef, @Optional() @Host() private parent?: FormComponent) {

  }

  ngOnInit() {
    if (!this.parent || !this.name) {
      return;
    }
    this.parent.add(this);
  }
  ngOnDestroy() {
    if (!this.parent) {
      return;
    }
    this.parent.remove(this);
  }
}
