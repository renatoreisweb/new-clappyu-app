export class Message{
    public status: 'sending' | 'sent' | 'error' = 'sending';
    public childsAttachments: Message[] = [];
    public attachment: {
        name: string;
        url: string;
    } = null;
    public read: {
        token: string;
        time: Date;
    }[] = [];

    constructor(
        public token: string,
        public text: string,
        public time: Date,
        public from: string,
        public avatar: string
        ){

    }
}