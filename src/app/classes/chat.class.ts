import { Message } from "./message.class";

export class Chat{
    public messages: Message[] = [];
    public participants: {
        token: string;
        avatar: string;
        type: number;
        username: string;
    }[] = [];
    
    constructor(
        public token: string, 
        public state: number,
        public event: {
            token: string;
        },
        public unread: number
        ){

    }



    get lastMessage(){
        return this.messages[this.messages.length - 1] || null;
    }

    get avatar(){
        return '';
    }
    get name(){
        return '';
    }
    get type(){
        return '';
    }
}