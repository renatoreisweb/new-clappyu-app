import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule , RouteReuseStrategy, DetachedRouteHandle } from '@angular/router';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { PublicModule } from './routes/public/public.module';
import { PrivateModule } from './routes/private/private.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { ModalComponent } from './components/modal/modal.component';
import { ApiService } from './services/api.service';

export class CustomReuseStrategy implements RouteReuseStrategy {
  static cache = {};
  constructor() { }
  shouldDetach(route: any): boolean {
    return route.data.cache && route.component;
  }
  store(route: any, handle: any): void {
    CustomReuseStrategy.cache[route._routerState.url] = handle;
  }
  shouldAttach(route: any): boolean {
    return CustomReuseStrategy.cache[route._routerState.url];
  }
  retrieve(route: any): DetachedRouteHandle {
    return CustomReuseStrategy.cache[route._routerState.url] as DetachedRouteHandle;
  }
  shouldReuseRoute(future: any, curr: any): boolean {
    return future.routeConfig === curr.routeConfig;
  }

  static clear(){
    CustomReuseStrategy.cache = [];
  }
}


@NgModule({
  declarations: [AppComponent, ModalComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    SharedModule,
    PublicModule,
    PrivateModule,
    RouterModule.forRoot([
      PublicModule.route,
      PrivateModule.route
    ], {
        paramsInheritanceStrategy: 'always'
      })],
  providers: [
    ApiService,    
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: CustomReuseStrategy }],
  bootstrap: [AppComponent]
})
export class AppModule { }
