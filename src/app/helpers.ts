export function toBase64(file: File) : Promise<any>{
    return new Promise((resolve, reject)=>{
        var fileReader  = new FileReader();
        fileReader.onload = function(e){
            resolve(fileReader.result);
        };
        fileReader.readAsDataURL(file);
    });
}